var config = {
    REDISURL: "redis://129.194.71.205:6379",
    PORT: "8080",
    PROTEIN_DB: "./resources/database/peptide_db.json",
    PEP_PROT_MAP: "./resources/json/human_peptide_protein_map.json",
    PROT_GO_MAP: "./resources/json/human_protein_go_map.json",
    GO_DICT: "./resources/json/go_dictionary.json",
    GO_LIST: "./resources/json/go_list.json",
    UNIPROT_DICT: "./resources/json/uniprot_dictionary.json",
    UNIPROT_LIST: "./resources/json/uniprot_list.json",
};

// later we should switch from hard coded
// configuration to environmental variables
/*
 //getEnv('REDISURL'),
 //getEnv('PORT')
function getEnv(variable){
    if (process.env[variable] === undefined){
        throw new Error('You must create an environment variable for ' + variable);
    }

    return process.env[variable];
};*/

module.exports = config;
