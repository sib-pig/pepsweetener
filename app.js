var https = require('https'),
    express = require('express'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    config = require('./conf'),
    repo = require('./controllers/repository'),
    loki = require('./controllers/loki');

var app = express();

app.use("/", express.static(__dirname + '/app'));
app.use("/components", express.static(__dirname + '/components'));
app.use("/dist", express.static(__dirname + '/dist'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.set('port', config.PORT);
//app.set('golist', loki.getGoList());

app.get('/proteins/:peptide', function(req, res) {
    var peptide = req.params.peptide;
    loki.ajaxProteins(peptide, res);
});

/*app.get('/uniprotList', function(req, res){
    var uniprotList = loki.getUniprotList();
    json = JSON.stringify(uniprotList);
    res.send('_uniprot('+json+')');
});

app.get('/golist', function(req, res){
    var golist = loki.getGoList();
    json = JSON.stringify(golist);
    res.send('_go('+json+')');
});*/

app.post('/search', function(req, res) {
    var mass = req.body.mass;
    var toleranceUnit = req.body.toleranceUnit;
    var toleranceValue = req.body.toleranceValue;
    var range = JSON.parse(req.body.range);
    var proteinFilter = '';//[req.body.proteinFilter];
    var goTermFilter = '';//[req.body.goFilter];
    if(req.body.proteinFilter!=''){
        for (var i=0; i<proteinFilter.length; i++) {
            proteinFilter[i] = loki.getUniprot(proteinFilter[i]);
        }
    };
    if(req.body.goFilter!=''){
        for (var i=0; i<goTermFilter.length; i++) {
            goTermFilter[i] = loki.getGoCodeByName(goTermFilter[i]);
        }
    };
    repo.fetchGlycopeptides(mass, toleranceUnit, toleranceValue, range, proteinFilter, goTermFilter, res);
});

var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});
