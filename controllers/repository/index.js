var client = require('../redis');
var loki = require('../loki');

module.exports.fetchGlycopeptides = fetchGlycopeptides;

function fetchGlycopeptides(queryMass, toleranceUnit, toleranceValue, ranges, proteinFilterList, goFilterList, res) {
    //TODO
    //ATTENTION! this is very bad and temporary fix, the redis dataset needs to be fixed
    queryMass = Number(queryMass)+18.0105647;
    if (toleranceUnit=="Da"){
        var daTolerance = Number(toleranceValue);
    } else {
        var daTolerance = ppmToDalton(queryMass, toleranceValue);
    }
    var minMass = queryMass - daTolerance;
    var maxMass = queryMass + daTolerance;
    var bucketMass = Math.round( queryMass * 10 );
    var minBucketMass = Math.round( minMass * 10 );
    var maxBucketMass = Math.round( maxMass * 10 );
    if (minBucketMass==maxBucketMass){
        client.lrange(bucketMass, 0, -1, function (err, reply) {
            if (err !== null) {
                res.send({ ok: false, message: 'error while fetching' });
            } else {
                var glycoPepResult = new Array();
                reply.forEach(function (glycopeptide){
                    var proteinFilterPass = proteinFilterMatch(proteinFilterList, loki.getProteins(glycopeptide.split(",")[0]));
                    var goFilterPass = goFilter(goFilterList, glycopeptide.split(",")[0]);
                    if(checkMass(glycopeptide.split(",")[2], minMass, maxMass) && proteinFilterPass && goFilterPass){
                        glycoPepResult.push(glycopeptide);
                    }
                })
            }
            res.send(generateD3input(glycoPepResult, queryMass, ranges));
        });
    }else{
        var multi = client.multi();
        while (minBucketMass!=maxBucketMass+1){
            var err;
            var glycoPeptideReply;
            multi.lrange(minBucketMass, 0, -1);
            minBucketMass +=1;
        }
        multi.exec(function(err, reply) {
            if (err !== null) {
                res.send({ ok: false, message: 'error while fetching' });
            } else {
                var glycoPepResult = new Array();
                reply.forEach(function (glycopeptideList){
                    glycopeptideList.forEach(function (glycopeptide) {
                        var proteinFilterPass = proteinFilterMatch(proteinFilterList, loki.getProteins(glycopeptide.split(",")[0]));
                        var goFilterPass = goFilter(goFilterList, glycopeptide.split(",")[0]);
                        if(checkMass(glycopeptide.split(",")[2], minMass, maxMass) && proteinFilterPass && goFilterPass) {
                            glycoPepResult.push(glycopeptide);
                        }
                    });
                })
            }
            res.send(generateD3input(glycoPepResult, queryMass, ranges));
        });

    }
};

function checkMass(mass, min, max){
    if (min <= mass && mass <= max){
        return true;
    }else{
        return false;
    }
}

function generateD3input(glycopeptideList, queryMass, ranges){
    var peptideList = [];
    var glycanList = [];
    var glycopeptideMap = [];
    if(glycopeptideList.length > 0) {
        var peptideCounter = 0;
        var glycanCounter = 0;
        glycopeptideList.forEach(function (glycopeptide) {
            glycopeptideArray = glycopeptide.split(",");
            var peptide = glycopeptideArray[0];
            if(!validateGlycan(glycopeptideArray[1],ranges)){
                return;
            }
            var glycan = glycopeptideArray[1].replace(":", "\:");
            var glycopeptideMass = glycopeptideArray[2];
            var peptideExists = contains(peptideList, peptide);
            var glycanExists = contains(glycanList, glycan);
            if(peptideExists!=-1){
                peptideIdx = peptideExists;
            }else{
                peptideCounter = peptideCounter+1;
                peptideIdx = peptideCounter;
                peptideList.push(peptide);
            }
            if(glycanExists!=-1){
                glycanIdx = glycanExists;
            }else{
                glycanCounter = glycanCounter+1;
                glycanIdx = glycanCounter;
                glycanList.push(glycan);
            }
            //TODO
            //this is very bad and temporary fix - remove water molecule
            glycopeptideMap.push({'peptide': peptideIdx, 'glycan': glycanIdx, 'value': daltonToPpm(queryMass, Math.abs(queryMass-glycopeptideMass)), 'mass': glycopeptideMass-18.0105647});
        });
    }
    //TODO
    //this is very bad and temporary fix - remove water molecule
    return({'mass': String(queryMass-18.0105647), 'data': {'peptides': peptideList, 'glycans': glycanList, 'map': glycopeptideMap}});
}

function goFilter(filterList, peptide){
    if(filterList==''){
        return true;
    }else{
        var proteinList = loki.getProteins(peptide);
        for (var i = 0; i < proteinList.length; i++)
             var goList = loki.getGoByProtein(proteinList[i]);
        for(var j = 0; j < filterList.length; j++){
            if(goList.indexOf(filterList[j]) != -1){
                return true;
            }
        }
    }
    return false;
}

function proteinFilterMatch(proteinFilterList, proteinList){

    if(proteinFilterList==''){
        return true;
    } else{
        for(var i = 0; i < proteinList.length; i++){
            if(proteinFilterList.indexOf(proteinList[i]) != -1){
                return true;
            }
        }
    }
    return false;
}

function ppmToDalton(mass, ppm){
    return (mass*ppm)/1000000;
}

function daltonToPpm(mass, da){
    return (da*1000000)/mass;
}

function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return i+1;
        }
    }
    return -1;
}

validateGlycan = function (glycanString,rangeList) {
    var glycan = createGlycan(glycanString);
    for(var range in rangeList){
        var minRange = Number(rangeList[range].minRange);
        var presence = Boolean(rangeList[range].presence);
        if(presence){   //Not possible in the range list.
            if(minRange > 0 && !glycan.hasOwnProperty(range) ){
                return false;
            }
            if(minRange > 0 && minRange > glycan[range] ){
                return false;
            }
        } else {
            if(glycan.hasOwnProperty(range)){
                return false;
            }
        }
    }
    return true;
};

createGlycan = function (glycanString) {
    var tokens = glycanString.split("|");
    var glycan = { };
    tokens.forEach(function (item) {
        var monosaccharide = item.split(":");
        glycan[monosaccharide[0]] = monosaccharide[1];
    })
    return glycan;
};
