var config = require('../../conf'),
    loki = require('lokijs'),
    fs = require('fs');

var proteinDB = new loki(config.PROTEIN_DB,
    {
        autoload: true
    });

loadProteinMap();
//loadGoList();
//loadGoMap();
//loadGoDict();
//loadUniprotList();
//loadUniprotDict();


module.exports.getProteins = getProteins;

function getProteins(peptide){
    var map = proteinDB.getCollection('protein_map');
    var query = map.by("peptide", peptide);
    return query.proteins;
}

module.exports.ajaxProteins = ajaxProteins;

function ajaxProteins(peptide, res){
    var map = proteinDB.getCollection('protein_map');
    var query = map.by("peptide", peptide);
    res.send({'map':query.proteins});
    //res.send('_proteins(\'{"map": ["'+query.proteins+'"]}\')');
}

module.exports.getGoList = getGoList;

function getGoList(){
    var map = proteinDB.getCollection('go_list');
    var query = map.get(1);
    return query.terms;
}

module.exports.getGoByProtein = getGoByProtein;

function getGoByProtein(protein){
    var map = proteinDB.getCollection('go_map');
    var query = map.by("protein", protein);
    if(query){
        return query.GO_terms;
    }
    return [];
}

module.exports.getUniprotList = getUniprotList;

function getUniprotList(){
    var map = proteinDB.getCollection('uniprot_list');
    var query = map.get(1);
    return query.names;
}

module.exports.getUniprot = getUniprot;

function getUniprot(proteinName){
    var map = proteinDB.getCollection('uniprot_dict');
    var query = map.by("name", proteinName);
    if(query){
        return query.id;
    }
    return [];
}

module.exports.getGoCodeByName = getGoCodeByName;

function getGoCodeByName(name){
    var map = proteinDB.getCollection('go_dict');
    var query = map.by("name", name);
    if(query){
        return query.id;
    }
    return "";
}

function loadProteinMap(){
    var map = proteinDB.addCollection('protein_map', {unique: ['peptide']});
    var jsonString = fs.readFileSync(config.PEP_PROT_MAP, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}

function loadGoMap(){
    var map = proteinDB.addCollection('go_map', {unique: ['protein']});
    var jsonString = fs.readFileSync(config.PROT_GO_MAP, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}

function loadGoList(){
    var map = proteinDB.addCollection('go_list');
    var jsonString = fs.readFileSync(config.GO_LIST, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}

function loadGoDict(){
    var map = proteinDB.addCollection('go_dict', {unique: ['id', 'name']});
    var jsonString = fs.readFileSync(config.GO_DICT, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}

function loadUniprotDict(){
    var map = proteinDB.addCollection('uniprot_dict', {unique: ['name']});
    var jsonString = fs.readFileSync(config.UNIPROT_DICT, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}

function loadUniprotList(){
    var map = proteinDB.addCollection('uniprot_list');
    var jsonString = fs.readFileSync(config.UNIPROT_LIST, 'utf8');
    map.insert(JSON.parse(jsonString));
    proteinDB.save();
}
