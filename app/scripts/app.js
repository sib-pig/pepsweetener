(function(document) {
    'use strict';

    var app = document.querySelector('#app');

    app.baseUrl = "/pepsweetener/app/";
    // Listen for template bound event to know when bindings
    // have resolved and content has been stamped to the page
    app.addEventListener('dom-change', function() {
    });

    // See https://github.com/Polymer/polymer/issues/1381
    window.addEventListener('WebComponentsReady', function() {
        // imports are loaded and elements have been registered
        var loadEl = document.getElementById('splash');
        loadEl.addEventListener('transitionend', loadEl.remove);
        document.body.classList.remove('loading');
    });

    app._closeDrawer = function() {
        app.$.drawerPanel.closeDrawer;
    };

    app._computeStyle = function(largeScreen) {
        return largeScreen ? 'large' : ' small';
    };

})(document);

jumpToHeader = function(headerId) {
    document.getElementById(headerId).scrollIntoView(true);
};

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-77270237-6', 'auto');
ga('send', 'pageview');
